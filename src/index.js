// TODO: Publish to NPM
// TODO: Throttle
// TODO: import eval scripts for copyq from template files
// TODO: check on initialize whether copyq is installed and in proper version
// TODO: option to filter by mimetype
// TODO: option to remove from clipboard history
// TODO: option to pin to clipboard history
// TODO: look into tags and tabs
// TODO: add settings for defining trigger
import { execSync } from 'child_process';
import icon from '../assets/copyq.png'

export const keyword = 'cq';
export const name = 'CopyQ';

const IMAGE = Symbol('Image');
const PLAIN = Symbol('Plain');
const HTML = Symbol('HTML');

const mimeTypesToSymbol = {
  'image/png': IMAGE,
  'text/plain': PLAIN,
  'text/html': HTML,
}

function formEntriesScriptMatch(str, maxEntries = 20) {
  const strScript = [
    'var result=[];',
    `var match = "${str}";`,
    `for (var i = 0; i < Math.min(${maxEntries}, size()); ++i) {`,
    'if (str(read(i)).search(new RegExp(match, "i")) !== -1) {',
    'var obj = {};',
    'obj.row = i;',
    'obj.mimetypes = str(read("?", i)).split("\\\\n");',
    'obj.mimetypes.pop();',
    'obj.text = str(read(i));',
    'obj.value = str(read(obj.mimetypes[0], i));',
    'result.push(obj);',
    '}}',
    'JSON.stringify(result);',
  ];

  return strScript.join(' ');
}

function formEntriesScript(maxEntries = 20) {
  const strScript = [
    'var result = [];',
    `for (var i = 0; i < Math.min(${maxEntries}, size()); ++i) {`,
    'var obj = {};',
    'obj.row = i;',
    'obj.mimetypes = str(read("?", i)).split("\\\\n");',
    'obj.mimetypes.pop();',
    'obj.text = str(read(i));',
    'if(obj.mimetypes[0] ==="image/png")',
    'obj.value = toBase64(read(obj.mimetypes[0], i));',
    'else',
    'obj.value = str(read(obj.mimetypes[0], i));',
    'result.push(obj);',
    '}',
    'JSON.stringify(result);',
  ];

  return strScript.join(' ');
}

export const fn = async ({ actions, term, display, settings }) => {
  if (!term.startsWith('cq ')) {
    return;
  }

  let entries;
  if (term.length > 3) {
    const match = term.substr(3);
    entries = JSON.parse(execSync(`copyq -e '${formEntriesScriptMatch(match, settings.maxEntries)}'`).toString());
  } else {
    entries = JSON.parse(execSync(`copyq -e '${formEntriesScript(settings.maxEntries)}'`).toString());
  }

  const results = entries.map((entry, i) => {
    const mimeType = mimeTypesToSymbol[entry.mimetypes[0]];
    return {
      title: mimeType === IMAGE ? `${entry.row}: (IMAGE)` : `${entry.row}: ${entry.text.substr(0,50)}`,
      subtitle: entry.mimetypes,
      icon,
      clipboard: entry.text,
      onSelect: () => execSync(`copyq -e "select(${entry.row})"`),
      getPreview: () => {
        if (mimeType === IMAGE) {
          const imageData = `data:image/png;base64,${entry.value}`;
          return `<img src=${imageData}></img>`;
        }
        return `<div>${entry.value}</div>`;
      }
    }
  });

  display(results);
}

export const settings = {
  maxEntries: {
    label: 'Max. entries',
    description: 'Maximum amount of CopyQ entries to return.',
    type: 'number',
    defaultValue: 20
  }
}
